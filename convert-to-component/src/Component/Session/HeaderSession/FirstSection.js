import React, { Component } from 'react';
import Service from '../BodySessions/Service';


export default class Firstsection extends Component {
  render() {
    return (
      <section className="page-section" id="services">
        <div className="container">
          <div className="row">
            <div className="col-lg-12 text-center">
              <h2 className="section-heading text-uppercase">Services</h2>
              <h3 className="section-subheading text-muted">Lorem ipsum dolor sit amet consectetur.</h3>
            </div>
          </div>
          <div className="row text-center">
          <Service titleService="E-Commerce" classIcon="fas fa-shopping-cart fa-stack-1x fa-inverse"></Service>
          <Service titleService="Responsive Design" classIcon="fas fa-laptop fa-stack-1x fa-inverse" ></Service>
          <Service titleService="Web Security" classIcon="fas fa-lock fa-stack-1x fa-inverse"></Service>
          </div> 
        </div>
      </section>

    );
  }
}