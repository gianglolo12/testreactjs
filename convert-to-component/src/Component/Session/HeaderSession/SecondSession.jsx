import React, { Component } from 'react';
import Porfolio from '../BodySessions/Porfolio';
import data from '../../../data/data'

export default class SecondSession extends Component {
  arr= data;
 renderPorfolio= ()=>{
   return this.arr.map((item,index)=> <Porfolio key={index} imgTitle={item.imgTitle} nameTitle={item.nameTitle} smallTitle={item.smallTitle}></Porfolio>)
 }

  // renderProf = () => {
  //   return this.arr.map((item, index) => {
  //     return (
  //       <Porfolio key={index} imgTitle={item.imgTitle} nameTitle={item.nameTitle} smallTitle={item.smallTitle}></Porfolio>
  //     )
  //   })
  // }
  render() {
    return (
      <section className="bg-light page-section" id="portfolio">
        <div className="container">
          <div className="row">
            <div className="col-lg-12 text-center">
              <h2 className="section-heading text-uppercase">Portfolio</h2>
              <h3 className="section-subheading text-muted">Lorem ipsum dolor sit amet consectetur.</h3>
            </div>
          </div>
          <div class="row">
         
          {this.renderPorfolio()}
          </div>
        </div>
      </section>

    );
  }
}