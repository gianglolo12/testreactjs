import React, { Component } from 'react';

export default class Porfolio extends Component {
  constructor(props) {
    super(props);
    this.state = {
      status: true
    }
  }
  renderButton = () => (

    <div className="btn-group" role="group" aria-label="Basic example">
      <button onClick={() => this.clickEdit()} type="button" className="btn btn-secondary">Edit</button>
      <button type="button" className="btn btn-success">Remove</button>
    </div>
  )
  renderInput = () => (
    <div className="input-group mt-1">
      <input type="text" className="form-control" placeholder="What do you want?"defaultValue={this.props.nameTitle}
        aria-describedby="button-addon2" />
      <div className="input-group-append">
        <button onClick={() => this.clickSave()} className="btn btn-danger" type="button" id="button-addon2">Save</button>
      </div>
    </div>
  )

  checkDisplay = () => {
    if (this.state.status === true) {
      return this.renderButton()
    } else {
      return this.renderInput()
    }
  }
  clickEdit = () => {
    this.setState({ status: false });
  }
  clickSave = () => {
    this.setState({ status: true });
  }
  render() {
    return (
      <div className="col-md-4 col-sm-6 portfolio-item">
        <a className="portfolio-link" data-toggle="modal" href="#portfolioModal1">
          <div className="portfolio-hover">
            <div className="portfolio-hover-content">
              <i className="fas fa-plus fa-3x"></i>
            </div>
          </div>
          <img className="img-fluid" src={this.props.imgTitle} alt="img" />
        </a>
        <div className="portfolio-caption">
          <h4>{this.props.nameTitle}</h4>
          <p className="text-muted">{this.props.smallTitle}</p>
          {/* Add Button */}
          {this.checkDisplay()}

          {/* add Input */}


        </div>

      </div>

    );
  }
}

