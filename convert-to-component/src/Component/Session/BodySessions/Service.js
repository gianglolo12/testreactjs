import React, { Component } from 'react';
export default class Service extends Component {
  render() {
    return (
      <div className="col-md-4">
        <span className="fa-stack fa-4x">
          <i className="fas fa-circle fa-stack-2x"style={{color:'#fed136'}} />
          <i className={this.props.classIcon}  />
        </span>
        <h4 className="service-heading">{this.props.titleService}</h4>
        <p className="text-muted">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Minima maxime quam architecto quo inventore harum ex magni, dicta impedit.</p>
      </div>

    );
  }
}