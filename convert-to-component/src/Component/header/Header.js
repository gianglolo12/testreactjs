import React, { Component } from 'react';

export default class Header extends Component { 
  alertReact=(name)=>{
    alert(name)
  }

  render() {
    return (
      <header className="masthead">
  <div className="container">
    <div className="intro-text">
      <div className="intro-lead-in">Welcome To Our Studio!</div>
      <div className="intro-heading text-uppercase">It's Nice To Meet Giang</div>
      <button className="btn btn-primary btn-lg text-uppercase js-scroll-trigger" onClick={this.alertReact.bind(this,'Hello! Nice to meet You')}>Click Me!!</button>
    </div>
  </div>
</header>

    );
  }
}

