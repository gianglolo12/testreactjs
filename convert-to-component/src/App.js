import React from 'react';
// import logo from './logo.svg';
import './App.css';
import TopMenu from './Component/topMenu/topMenu';
import Header from './Component/header/Header';
import Firstsection from './Component/Session/HeaderSession/FirstSection';
import SecondSession from './Component/Session/HeaderSession/SecondSession';

export default function App() {
  return (
    <div className="App">
      <TopMenu></TopMenu>
      <Header></Header>
      <Firstsection></Firstsection>
      <SecondSession></SecondSession>
    </div>
  );
} 

