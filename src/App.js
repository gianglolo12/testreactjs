import { Component } from 'react';
import React from 'react';
import logo from './logo.svg';
import './App.css';
const arr=["Cam","Quýt","Đào","Bưởi","Táo"];
const arr2= arr.map((item)=>(
    <li>{item}</li>
));
class Arr2 extends Component {
  render() {
    return (
      <div>  
        <ul>
        {arr2}
          </ul> 
        
      </div>
    );
  }
}

// Use props with First Option
// let NumberOne=(props)=>(
//   <div>
//   <div className="card" style={{width: 400}}>
//     <img className="card-img-top" src={props.addImg}  alt="Img"/>
//     <div className="card-body">
//       <h4 className="card-title">{props.tieude}</h4>
//       <button className="btn btn-primary">Nope!</button>
//     </div>
//   </div>

//   </div>
// )
//--------------------------------------------------------------------
// Use props with Second Option ( Class)
      class NumberTwo extends Component {
        render() {
          return (
            <div>
                 <div className="card" style={{width: 400}}>
                  <img className="card-img-top" src={this.props.addImg}  alt="Img"/>
                  <div className="card-body">
                    <h4 className="card-title">{this.props.tieude}</h4>
                    <button className="btn btn-primary">Nope!</button>
                  </div>
                </div>
            </div>
          );
        }
      }
      
let Header=()=>(
<header className="App-header container-fluid">
        <img src= {logo} className="App-logo" alt="logo" />
        <h1>Giang</h1>
        <Arr2></Arr2>
      </header>
)
let Body=()=>(
  <body className="container mt-2 g-body ">
    <div className="row"> 
    <div className="col-lg-8 offset-4 text-center">
      <NumberTwo tieude="Dù là người ít nói tới đâu thì lỡ chẳng may crush một cô gái, con trai cũng sẽ cố gắng tìm cách bắt chuyện" addImg="https://kenh14cdn.com/thumb_w/620/2019/9/29/artboard-1-15697750787281056265719.png"></NumberTwo>
      </div>
    </div>
    <div className="row">
    <div className="col-lg-8 offset-4 mt-2 text-center">
    <NumberTwo tieude="Tụi con trai lắm khi trẻ con lắm, thích người ta là nhất định phải gây chú ý bằng những trò ngốc xít cơ" addImg="https://kenh14cdn.com/thumb_w/620/2019/9/29/artboard-1-copy-3-15697750787001152380226.png"></NumberTwo>
    </div>

    </div>
    <div className="row">
    <div className="col-lg-8 offset-4 text-center mt-2">
      <NumberTwo tieude="Không thích thì ai buồn để ý việc hôm nay son của bạn có màu khác hôm qua hay tóc mái của bạn vừa ngắn đi 1cm cơ chứ"addImg="https://kenh14cdn.com/thumb_w/620/2019/9/29/artboard-1-copy-1569775078725320338864.png" ></NumberTwo>
      </div>
      
    </div>
    <div className="row">
    <div className="col-lg-8 offset-4 text-center mt-2">
      <NumberTwo tieude="Bạn lúc ấy giống như vầng hào quang, dù bạn ở đâu thì người ta cũng tăm tia được bạn ngay lập tức"addImg="https://kenh14cdn.com/thumb_w/620/2019/9/29/artboard-1-copy-4-1569775078704664341483.png" ></NumberTwo>
      </div>
      
    </div>
      </body>
)
class App extends Component {
  render() {
    return (
      <giang className="App">
        <Header></Header>
        <Body></Body>
        
    </giang>
    )
  }
}


export default App;
